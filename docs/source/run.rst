Running opgpcard
=================

To know which arguments you can run ``opgpcard`` with, run::

    opgpcard -h

At the time of writing this documention, the output of that command is::

    usage: opgpcard [-h] [-d] [-o OUTPUTDIR] [-v] [-f FIRSTNAME] [-l LASTNAME]
                [-p FINGERPRINT] [-s] [-m MAIL]

    optional arguments:
      -h, --help            show this help message and exit
      -d, --debug           Set logging level to debug
      -o OUTPUTDIR, --outputdir OUTPUTDIR
      -v, --version         version
      -f FIRSTNAME, --firstname FIRSTNAME
      -l LASTNAME, --lastname LASTNAME
      -p FINGERPRINT, --fingerprint FINGERPRINT
      -s, --localsign
      -m MAIL, --mail MAIL
