opgpcard Python API Reference
==============================

opgpcard package
=================

Submodules
----------

opgpcard\.opgpcard module
-------------------------

.. automodule:: opgpcard.opgpcard
    :members:
    :undoc-members:
    :show-inheritance:

opgpcard\.conf module
-------------------------

.. automodule:: opgpcard.conf
    :members:
    :undoc-members:
    :show-inheritance:

opgpcard\.gpg_utils module
--------------------------

.. automodule:: opgpcard.gpg_utils
    :members:
    :undoc-members:
    :show-inheritance:

opgpcard\.qrsvg module
----------------------------

.. automodule:: opgpcard.qrsvg
    :members:
    :undoc-members:
    :show-inheritance:

opgpcard\.vcard_utils module
-----------------------------

.. automodule:: opgpcard.vcard_utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: opgpcard
    :members:
    :undoc-members:
    :show-inheritance:
